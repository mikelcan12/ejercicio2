<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\conditions\BetweenCondition;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(dorsal) FROM ciclista',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(dorsal)'],
                    "titulo" => "Consulta 1 con DAO",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT COUNT(dorsal) FROM ciclista",
        ]);
        
    }
     public function actionConsulta1a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('COUNT(dorsal) as contador'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ["contador"],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "Número de ciclistas que hay",
                    "sql" => "SELECT COUNT(dorsal) FROM ciclista",
        ]);
    }
     public function actionConsulta2() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(*) FROM ciclista WHERE nomequipo="Banesto"',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(*)'],
                    "titulo" => "Consulta 2 con DAO",
                    "enunciado" => "Número de ciclistas que hay del equipo Banesto",
                    "sql" => "SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
        
    }
     public function actionConsulta2a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('COUNT(*) as contador')
                    ->where('nomequipo="Banesto"'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ["contador"],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "Número de ciclistas que hay del equipo Banesto",
                    "sql" => "SELECT COUNT(*) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
    public function actionConsulta3() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT AVG(edad) FROM ciclista',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['AVG(edad)'],
                    "titulo" => "Consulta 3 con DAO",
                    "enunciado" => "Edad media de los ciclistas",
                    "sql" => "SELECT AVG(edad) FROM ciclista",
        ]);
        
    }
     public function actionConsulta3a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('AVG(edad) as contador'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ["contador"],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "Edad media de los ciclistas",
                    "sql" => "SELECT AVG(edad) FROM ciclista",
        ]);
    }
    public function actionConsulta4() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT AVG(edad) FROM ciclista WHERE nomequipo="Banesto"',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['AVG(edad)'],
                    "titulo" => "Consulta 4 con DAO",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => "SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
        
    }
     public function actionConsulta4a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('AVG(edad) as contador')
                    ->where('nomequipo="Banesto"'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ["contador"],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => "SELECT AVG(edad) FROM ciclista WHERE nomequipo='Banesto'",
        ]);
    }
     public function actionConsulta5() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipo',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','AVG(edad)'],
                    "titulo" => "Consulta 5 con DAO",
                    "enunciado" => "La edad media de los ciclistas por cada equipo",
                    "sql" => "SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipo",
        ]);
        
    }
     public function actionConsulta5a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('nomequipo, AVG(edad) as contador')
                    ->groupBy('nomequipo'),
            
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','contador'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "La edad media de los del equipo Banesto",
                    "sql" => "SELECT nomequipo, AVG(edad) FROM ciclista GROUP BY nomequipos",
        ]);
    }
     public function actionConsulta6() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'select nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','COUNT(*)'],
                    "titulo" => "Consulta 6 con DAO",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => "select nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
        
    }
     public function actionConsulta6a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('nomequipo, COUNT(*) as contador')
                    ->groupBy('nomequipo'),
            
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo','contador'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "El número de ciclistas por equipo",
                    "sql" => "select nomequipo, COUNT(*) FROM ciclista GROUP BY nomequipo",
        ]);
    }
      public function actionConsulta7() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(*) FROM puerto',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(*)'],
                    "titulo" => "Consulta 7 con DAO",
                    "enunciado" => " El número total de puertos",
                    "sql" => " SELECT COUNT(*) FROM puerto",
        ]);
        
    }
     public function actionConsulta7a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find()
                    ->select('COUNT(*) as contador'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['contador'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "El número total de puertos",
                    "sql" => "SELECT COUNT(*) FROM puerto",
        ]);
    }
 public function actionConsulta8() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT COUNT(nompuerto) FROM puerto WHERE altura>1500',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['COUNT(nompuerto)'],
                    "titulo" => "Consulta 8 con DAO",
                    "enunciado" => "El número total de puertos mayores de 1500",
                    "sql" => "SELECT COUNT(nompuerto) FROM puerto WHERE altura>1500",
        ]);
        
    }
     public function actionConsulta8a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Puerto::find()
                    ->select('COUNT(nompuerto) as contador')
                    ->where('altura>1500'),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['contador'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => " El número total de puertos mayores de 1500",
                    "sql" => "SELECT COUNT(nompuerto) FROM puerto WHERE altura>1500",
        ]);
    }
     public function actionConsulta9() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nomequipo  FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 9 con DAO",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql" => "SELECT nomequipo  FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
        
    }
     public function actionConsulta9a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('nomequipo')
                    ->groupBy("nomequipo")
                    ->having("COUNT(*)>4"),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => " Listar el nombre de los equipos que tengan más de 4 ciclistas",
                    "sql" => "SELECT nomequipo  FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>4",
        ]);
    }
    
     public function actionConsulta10() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT nomequipo  FROM ciclista where edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(nombre)>4',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta 10 con DAO",
                    "enunciado" => "Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" => "SELECT nomequipo  FROM ciclista where edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(nombre)>4",
        ]);
        
    }
     public function actionConsulta10a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Ciclista::find()
                    ->select('nomequipo')
                    ->where("edad BETWEEN 28 AND 32")
                    ->groupBy("nomequipo")
                    ->having("COUNT(*)>4"),
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['nomequipo'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => " Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
                    "sql" => "SELECT nomequipo  FROM ciclista where edad BETWEEN 28 AND 32 GROUP BY nomequipo HAVING COUNT(nombre)>4",
        ]);
    }
      public function actionConsulta11() {
            $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT e.dorsal, COUNT(*) FROM etapa e GROUP BY e.dorsal',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal','COUNT(*)'],
                    "titulo" => "Consulta 11 con DAO",
                    "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" => "SELECT e.dorsal, COUNT(*) FROM etapa e GROUP BY e.dorsal",
        ]);
        
    }
     public function actionConsulta11a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find()
                    ->select('dorsal, COUNT(*) as contador')
                    ->groupBy("dorsal"),
                   
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal','contador'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => "Indícame el número de etapas que ha ganado cada uno de los ciclistas",
                    "sql" => "SELECT e.dorsal, COUNT(*) FROM etapa e GROUP BY e.dorsal",
        ]);
    }
      public function actionConsulta12() {
            $dataProvider = new SqlDataProvider([
            'sql' => ' SELECT e.dorsal, COUNT(e.dorsal) FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1',
            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                    "titulo" => "Consulta 12 con DAO",
                    "enunciado" => "Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" => " SELECT e.dorsal, COUNT(e.dorsal) FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1",
        ]);
        
    }
     public function actionConsulta12a() {
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find()
                   ->select("dorsal")
                   ->groupBy("dorsal")
                   ->having("COUNT(*)>1"),

            'pagination' => [
                'pagesize' => 10,
            ]
        ]);

        return $this->render("resultado", [
                    "resultados" => $dataProvider,
                    "campos" => ['dorsal'],
                    "titulo" => "Consulta con Active Record",
                    "enunciado" => " Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
                    "sql" => " SELECT e.dorsal, COUNT(e.dorsal) FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1",
        ]);
    }
}

